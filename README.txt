==========================================================
Giftbox Mod v2.1 by sorcerykid

https://forum.minetest.net/viewtopic.php?f=9&t=19133


License of source code
----------------------------------------------------------

GNU Lesser General Public License v3 (LGPL-3.0)

Copyright (c) 2016-2017, Leslie E. Krause

Original source code by maikerumine:
https://github.com/maikerumine/just_test_tribute/blob/master/mods/mt_seasons/nodes.lua

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU Lesser General Public License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

http://www.gnu.org/licenses/lgpl-2.1.html



License of media (textures, sounds, and models)
----------------------------------------------------------

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)

	/models/giftbox.obj
	by Toby109tt

	/textures/present_bottom.png
	by lag01
	modified by maikerumine
	modified by sorcerykid

	/textures/present_side.png
	by lag01
	modified by maikerumine
	modified by sorcerykid

	/textures/present_top.png
	by lag01
	modified by maikerumine
	modified by sorcerykid

	/textures/giftbox_red.png
	by Toby109tt
	modified by sorcerykid

	/textures/giftbox_green.png
	by Toby109tt
	modified by sorcerykid

	/textures/giftbox_blue.png
	by Toby109tt
	modified by sorcerykid

	/textures/giftbox_cyan.png
	by Toby109tt
	modified by sorcerykid

	/textures/giftbox_magenta.png
	by Toby109tt
	modified by sorcerykid

	/textures/giftbox_yellow.png
	by Toby109tt
	modified by sorcerykid

	/textures/giftbox_black.png
	by Toby109tt
	modified by sorcerykid

	/textures/giftbox_white.png
	by Toby109tt
	modified by sorcerykid

You are free to:
Share — copy and redistribute the material in any medium or format.
Adapt — remix, transform, and build upon the material for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and
indicate if changes were made. You may do so in any reasonable manner, but not in any way
that suggests the licensor endorses you or your use.

No additional restrictions — You may not apply legal terms or technological measures that
legally restrict others from doing anything the license permits.

Notices:

You do not have to comply with the license for elements of the material in the public
domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary
for your intended use. For example, other rights such as publicity, privacy, or moral
rights may limit how you use the material.

For more details:
http://creativecommons.org/licenses/by-sa/3.0/
